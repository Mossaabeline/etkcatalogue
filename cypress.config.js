const { defineConfig } = require('cypress')

module.exports = defineConfig({
    viewportWidth: 2100,
    viewportHeight: 1600,
    projectId: "qe6dxs",
    e2e: {
        // We've imported your old cypress plugins here.
        // You may want to clean this up later by importing these.
        setupNodeEvents(on, config) {
            return require('./cypress/plugins/index.js')(on, config)
        },
        baseUrl: 'http://192.168.2.52:8586/home',
        specPattern: 'cypress/e2e/**/*.feature',

    },
})