// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// 
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })


// saisir et sans saisir designation 

import 'cypress-file-upload';

Cypress.Commands.add('menu', (etat) => {
    cy.fixture('donne.json').then(value => {
        if (etat == "Valider") {
            cy.get('#title').type(value.carte.designation)
            cy.wait(1000)
            cy.get('#remark').type(value.carte.Remarque)
            cy.wait(1000)
        } else if (etat == "NotValide") {
            cy.get('#remark').type(value.carte.Remarque)
            cy.wait(1000)
        }
    })
})

// verification les toast
Cypress.Commands.add('Verifytoast', (etattoast) => {
    if (etattoast == 'toastvalid') {
        cy.get('.Toastify__toast-body').should('have.text', 'La carte est ajoutée avec succès')
        cy.wait(4000)
    } else if (etattoast == 'toastnotvalid') {
        cy.get('.Toastify__toast-body > :nth-child(2)').should('have.text', "Le champs ''Désignation''  doit être rempli")
    } else if (etattoast == 'toastsuppression') {
        cy.get('.Toastify__toast-body > :nth-child(2)').should('have.text', 'La carte est supprimée avec succès')
        cy.wait(1000)
    }
})

// ajouter une carte
Cypress.Commands.add('button', (choixbtn) => {
    cy.fixture('toutbutton').then(cliker => {
        if (choixbtn == 'Ajouter un menu') {
            cy.get('span').contains(cliker.btn.ajouter).click()
            cy.wait(1000)
        } else if (choixbtn == 'Valider') {
            cy.contains(cliker.btn.btnvalider).click()
        } else if (choixbtn == 'Supprimer') {
            cy.get('button').contains(cliker.btn.delete).click()
            cy.wait(1000)
            cy.get('[class="sc-bczRLJ cueLih"]').click()


        } else if (choixbtn == 'points') {
            cy.get(cliker.btn.torispoints).eq(3).click()
            cy.wait(1000)
        } else if (choixbtn == ' Annuler') {
            cy.get('button').contains(cliker.btn.btnannuler).click()
        }
    })
})

// test design buttons sous ETK articles DB
Cypress.Commands.add('fontezise', (choixsize) => {
    cy.fixture('donne.json').then(desig => {
        if (choixsize == 'ETK|Article DB') {
            cy.get('h1').should('have.text', 'ETK|Article DB')
            cy.get('.sc-eCYdqJ').should("have.css", 'color', desig.sizeetk.color.color)
            cy.get('.sc-eCYdqJ').should("have.css", 'Margin', desig.sizeetk.Margin.margin)
            cy.get('.sc-eCYdqJ').should("have.css", "font-weight", desig.sizeetk.Weight.weight)
            cy.get('.sc-eCYdqJ').should("have.css", 'Padding', desig.sizeetk.Padding.padding)
            cy.get('.sc-eCYdqJ').should("have.css", 'line-height', desig.sizeetk.Height.height)
            cy.get('.sc-eCYdqJ').should("have.css", 'text-align', desig.sizeetk.text.texte)

        } else if (choixsize == 'Accueil') {
            cy.get('.sidebar-menu__clz > :nth-child(1) > a.d-flex').should('have.text', 'Accueil')
            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'color', desig.sizeacceuil.color.color)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'Margin', desig.sizeacceuil.Margin.margin)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'font-weight', desig.sizeacceuil.Weight.weight)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'line-height', desig.sizeacceuil.Height.height)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'cursor', desig.sizeacceuil.curse.cursor)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'text-align', desig.sizeacceuil.text.texte)

        } else if (choixsize == 'Catalogue') {
            cy.get('.sidebar-menu__clz > :nth-child(1) > a.d-flex').should('have.text', 'Accueil')
            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'color', desig.sizecatalogue.color.color)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'Margin', desig.sizecatalogue.Margin.margin)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'font-weight', desig.sizecatalogue.Weight.weight)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'line-height', desig.sizecatalogue.Height.height)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'cursor', desig.sizecatalogue.curse.cursor)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'text-align', desig.sizecatalogue.text.texte)

        } else if (choixsize == 'Modules') {
            cy.get('.sidebar-menu__clz > :nth-child(1) > a.d-flex').should('have.text', 'Accueil')
            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'color', desig.sizemodules.color.color)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'Margin', desig.sizemodules.Margin.margin)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'font-weight', desig.sizemodules.Weight.weight)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'line-height', desig.sizemodules.Height.height)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'cursor', desig.sizemodules.curse.cursor)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'text-align', desig.sizemodules.text.texte)

        } else if (choixsize == 'Configuration') {
            cy.get('.sidebar-menu__clz > :nth-child(1) > a.d-flex').should('have.text', 'Accueil')
            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'color', desig.sizeConfiguration.color.color)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'Margin', desig.sizeConfiguration.Margin.margin)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'font-weight', desig.sizeConfiguration.Weight.weight)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'line-height', desig.sizeConfiguration.Height.height)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'cursor', desig.sizeConfiguration.curse.cursor)

            cy.get(':nth-child(1) > a.d-flex > .d-flex > span').should("have.css", 'text-align', desig.sizeConfiguration.text.texte)

        } else if (choixsize == 'Liste des cartes ') {
            cy.get('.sidebar-menu__clz > :nth-child(2) > a.d-flex').should('have.text', 'Liste des cartes ')
            cy.get(':nth-child(2) > a.d-flex > .d-flex > span').should("have.css", 'color', desig.listecarte.color.color)
            cy.get(':nth-child(2) > a.d-flex > .d-flex > span').should("have.css", 'Margin', desig.listecarte.Margin.margin)
            cy.get(':nth-child(2) > a.d-flex > .d-flex > span').should("have.css", 'font-weight', desig.listecarte.Weight.weight)
            cy.get(':nth-child(2) > a.d-flex > .d-flex > span').should("have.css", 'line-height', desig.listecarte.Height.height)
            cy.get(':nth-child(2) > a.d-flex > .d-flex > span').should("have.css", 'cursor', desig.listecarte.curse.cursor)
            cy.get(':nth-child(2) > a.d-flex > .d-flex > span').should("have.css", 'text-align', desig.listecarte.text.texte)
        }
    })
})

// test design menu ouvet plus fermer
Cypress.Commands.add('menudesign', (menu) => {
    cy.fixture('donne.json').then(shoismenu => {
        if (menu == 'menu ouvert') {
            cy.get('[class="sidebar-menu__clz py-3"]').should('have.css', 'color', shoismenu.menuetk.Color.color)
            cy.get('[class="sidebar-menu__clz py-3"]').should('have.css', 'min-width', shoismenu.menuetk.minwidth.minwidth)
            cy.get('[class="sidebar-menu__clz py-3"]').should('have.css', 'font-size', shoismenu.menuetk.fontsize.fontsize)
            cy.get('[class="sidebar-menu__clz py-3"]').should('have.css', 'font-weight', shoismenu.menuetk.Weight.weight)
            cy.get('.sidebar-menu__clz').should('have.css', 'line-height', shoismenu.menuetk.Height.height)
            cy.get('[class="sidebar-menu__clz py-3"]').should('have.css', 'border-right', shoismenu.menuetk.border.borderrigth)
        } else if (menu == 'menu fermer') {
            cy.get('[class="sidebar-menu__clz py-3"]').click()
            cy.get('[class="sidebar-menu__clz py-3"]').should('have.css', 'color', shoismenu.menuetk.Color2.color)
            cy.get('.sidebar-menu__clz').should('have.css', 'font-size', shoismenu.menuetk.fontsizee.fontsizee)
            cy.get('.sidebar-menu__clz').should('have.css', 'font-weight', shoismenu.menuetk.Weight2.weight)
            cy.get('.sidebar-menu__clz').should('have.css', 'line-height', shoismenu.menuetk.Height2.height)
                // cy.get('[class="sc-bczRLJ sc-gKXOVf iJHBhA dJWFGW m-0 border-none__clz"]').click()
        }
    })
})

// designe page d'acceuil
Cypress.Commands.add('choixpages', (page) => {
    if (page == 'CATEGORIES') {
        cy.get('[class=" page-card-carte col-lg-4 col-xl-4 col-md-6 col-sm-12 col-xs-12 my-2"]').should('have.css', 'cursor', 'pointer').and('have.css', 'pointer-events', 'none').and('have.css', 'box-sizing', 'border-box').and('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'line-height', '24px').and('have.css', 'padding-right', '15px').and('have.css', 'padding-left', '15px')

    } else if (page == 'ARTICLES') {
        cy.get('[class=" page-card-carte col-lg-4 col-xl-4 col-md-6 col-sm-12 col-xs-12 my-2"]').should('have.css', 'cursor', 'pointer').and('have.css', 'pointer-events', 'none').and('have.css', 'box-sizing', 'border-box').and('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'line-height', '24px').and('have.css', 'padding-right', '15px').and('have.css', 'padding-left', '15px')

    } else if (page == 'BALISES') {
        cy.get('[class=" page-card-carte col-lg-4 col-xl-4 col-md-6 col-sm-12 col-xs-12 my-2"]').should('have.css', 'cursor', 'pointer').and('have.css', 'pointer-events', 'none').and('have.css', 'box-sizing', 'border-box').and('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'line-height', '24px').and('have.css', 'padding-right', '15px').and('have.css', 'padding-left', '15px')

    } else if (page == 'FLUX DE TRAVAIL') {
        cy.get('[class=" page-card-carte col-lg-4 col-xl-4 col-md-6 col-sm-12 col-xs-12 my-2"]').should('have.css', 'cursor', 'pointer').and('have.css', 'pointer-events', 'none').and('have.css', 'box-sizing', 'border-box').and('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'line-height', '24px').and('have.css', 'padding-right', '15px').and('have.css', 'padding-left', '15px')

    } else if (page == 'CHOISIR UNE CARTE') {
        cy.get('[class=" page-card-carte col-lg-4 col-xl-4 col-md-6 col-sm-12 col-xs-12 my-2"]').should('have.css', 'cursor', 'pointer').and('have.css', 'pointer-events', 'none').and('have.css', 'box-sizing', 'border-box').and('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'line-height', '24px').and('have.css', 'padding-right', '15px').and('have.css', 'padding-left', '15px')
    }
})

//visibilité boutons sous catalogue
Cypress.Commands.add('namebutton', (name) => {
    cy.fixture('souscatalogue').then(chosie => {
        if (name == 'Catalogue') {
            cy.get('span').contains(chosie.nombtn.nameCatalogue).should('be.visible')
            cy.wait(1000)
        } else if (name == 'Catégories') {
            cy.get('a').contains(chosie.nombtn.nameCatégories).should('be.visible')
        } else if (name == 'Articles') {
            cy.get('a').contains(chosie.nombtn.nameArticles).should('be.visible')
        } else if (name == 'Options') {
            cy.get('a').contains(chosie.nombtn.nameOptions).should('be.visible')

        } else if (name == 'Balises') {
            cy.get('a').contains(chosie.nombtn.nameBalises).should('be.visible')
        } else if (name == 'Workflow') {
            cy.get('a').contains(chosie.nombtn.nameEtapes).should('be.visible')
        } else if (name == 'Allergènes') {
            cy.get('a').contains(chosie.nombtn.nameAllergènes).should('be.visible')
        }
    })
})

// cliker button sous catalogue
Cypress.Commands.add('selectbtn', (choixnamebtn) => {
    cy.fixture('souscatalogue').then(namebutton => {
        if (choixnamebtn == 'Catalogue') {
            cy.get('span').contains(namebutton.nombtn.nameCatalogue).click()
        } else if (choixnamebtn == 'Catégories') {
            cy.get('a').contains(namebutton.nombtn.nameCatégories).click()
            cy.wait(1000)

        } else if (choixnamebtn == 'Liste des cartes ') {
            cy.get('span').contains(namebutton.nombtn.namelistedescartes).click()
            cy.wait(2000)

        } else if (choixnamebtn == 'Choisir') {
            cy.get('[class="sc-bczRLJ kewGiy"]').eq(0).contains(namebutton.nombtn.namechoisir).click()
        } else if (choixnamebtn == 'dropdown') {
            cy.get('[class="dropdown "]').click()
        } else if (choixnamebtn == 'Articles') {
            cy.get('a').contains(namebutton.nombtn.namearticle).click()
            cy.wait(2000)
        } else if (choixnamebtn == 'Ajouter un article') {
            cy.get('.sc-bczRLJ > span').click()
        } else if (choixnamebtn == 'Avancé') {
            cy.get(':nth-child(2) > .d-flex > span').click()
        } else if (choixnamebtn == ' Annuler') {
            //  cy.get('[class="sc-bczRLJ fuUxBu"]').contains(' Annuler').click()
            cy.get('.justify-content-end > .d-flex > :nth-child(1) > .sc-bczRLJ').click()
        }
    })
})

//test visibilte page categories
Cypress.Commands.add('testvisibilite', (visible) => {
    cy.fixture('souscatalogue').then(chosir => {
        if (visible == 'ETK CATALOGUE ') {
            cy.get('[href="home"]').contains(chosir.nombtn.barrecate).should('be.visible')
        } else if (visible == 'FRANCHISE_MOHAMED_2022') {
            cy.get('a').contains(chosir.nombtn.namefranchise).should('be.visible')
        } else if (visible == 'Texte à rajouter dans cette zone') {
            cy.get('h6').contains(chosir.nombtn.nametexterajouter).should('be.visible')
        } else if (visible == 'Ajouter une catégorie') {
            cy.get('[class="sc-bczRLJ sc-gsnTZi cueLih krVLbz"]').contains(chosir.nombtn.nameajoutercatégorie).should('be.visible')
        } else if (visible == 'Filtre(s)') {
            cy.get('[class="sc-iIPllB jnXHUI"]').contains(chosir.nombtn.namefiltre).should('be.visible')
        } else if (visible == 'Rechercher') {
            cy.get('[placeholder="Rechercher"]').should('be.visible')
                // } else if (visible == 'checkbox') {
                //     cy.get('table>thead>tr>th:eq(0)').should('be.visible')
        } else if (visible == 'Désignation') {
            cy.get('span').contains(chosir.nombtn.nameDésignation).should('be.visible')
                // cy.get('table>thead>tr>th:eq(1)').contains('Désignation').should('be.visible')
                // cy.get('table[class="MuiTable-root makeStyles-rawTable-1 css-rqglhn-MuiTable-root"]').contains('th', 'Désignation').should('be.visible')
        } else if (visible == 'Ordre') {
            cy.get('[class="MuiButtonBase-root MuiTableSortLabel-root makeStyles-tableSortLabel-10 css-1qgma8u-MuiButtonBase-root-MuiTableSortLabel-root"]').eq(1).contains(chosir.nombtn.nameordre).should('be.visible')
        } else if (visible == 'Image') {
            cy.get('[class="MuiButtonBase-root MuiTableSortLabel-root makeStyles-tableSortLabel-10 css-1qgma8u-MuiButtonBase-root-MuiTableSortLabel-root"]').contains(chosir.nombtn.nameImage).should('be.visible')
        } else if (visible == 'Couleur') {
            cy.get('.makeStyles-tableLabel-7').eq(1).contains(chosir.nombtn.namecoleur).should('be.visible')
        } else if (visible == 'Description') {
            cy.get('span').contains(chosir.nombtn.namedescriptions).should('be.visible')
        } else if (visible == 'Actions') {
            cy.get('div').contains(chosir.nombtn.nameaction).should('be.visible')
        } else if (visible == 'dropdown') {
            cy.get('table>thead>tr>th:eq(7)').should('be.visible')
        }
    })
})

//verification case selection tout 
Cypress.Commands.add('checkboxbtn', (check) => {
    cy.get('table>thead>tr>th:eq(0)').should('be.visible')
    cy.get('table>thead>tr>th:eq(0)').click()
    cy.get('table>thead>tr>th:eq(0)').click()
})

//verification nombre ligne, colonne et le numero de page catégories
Cypress.Commands.add('number', (lignenum) => {
    cy.fixture('souscatalogue').then(numero => {
        if (lignenum == 'ligne') {
            // calculer nombre de lignes methode 1
            cy.get('table>tbody>tr').then((value) => {
                    const taille = Cypress.$(value).length
                    cy.log(taille)
                    expect(taille).to.equal(numero.nombtn.numberligne)
                })
                // calculer nombre de lignes methode 2
                // cy.get('table>tbody').find('tr').should('have.length', 10)
        } else if (lignenum == 'colonne') {
            cy.get('table>tbody>tr:eq(1)>td').then((value) => {
                const taille = Cypress.$(value).length
                cy.log(taille)
                expect(taille).to.equal(numero.nombtn.numbercolone)
            })
        } else if (lignenum == 'Lignes par page') {
            cy.get('[class="d-flex align-items-center"]').contains(numero.nombtn.nameLignesparpage).should('be.visible')
        } else if (lignenum == 'Page 1 sur 13') {
            cy.get('span.d-flex').contains(numero.nombtn.namenbrpage).should('be.visible')
        }
    })
})

//verification contenue tableau catégories
Cypress.Commands.add('menudropdown', (popup) => {
    cy.fixture('souscatalogue').then(selec => {
        if (popup == 'dropdown-menu') {
            cy.get('[class="dropdown-menu show"]').should('be.visible')
        } else if (popup == 'Afficher tous:') {
            cy.get('label').eq(0).contains(selec.nombtn.nameAffichertous).should('be.visible')
        } else if (popup == 'Désignation') {
            cy.get('label').eq(1).contains(selec.nombtn.nameDésignation).should('be.visible')
        } else if (popup == 'Ordre') {
            cy.get('label').eq(2).contains(selec.nombtn.nameordre).should('be.visible')
        } else if (popup == 'Image') {
            cy.get('label').eq(3).contains(selec.nombtn.nameImage).should('be.visible')
        } else if (popup == 'Couleur') {
            cy.get('label').eq(4).contains(selec.nombtn.namecoleur).should('be.visible')
        } else if (popup == 'Description') {
            cy.get('label').eq(5).contains(selec.nombtn.namedescriptions).should('be.visible')
        }
    })
})

//pop up ajouter une categories
Cypress.Commands.add('ajoutercategories', (categories) => {
    cy.fixture('souscatalogue').then(categ => {
        if (categories == 'Ajouter une catégorie') {
            cy.get('[class="av-valid"]').contains(categ.nomcategries.nomajoutercategries).click()
        } else if (categories == 'Avancé') {
            cy.get('.pt-4 > div').contains(categ.nomcategries.nomavanvé).click()
                // cy.wait(5000)
        } else if (categories == 'close') {
            cy.get('[class="close"]').click()
        }
    })
})

//Visibilité fenetre ajouter une catégorie
Cypress.Commands.add('fentreajouter', (fenetreajoutcat) => {
    cy.fixture('souscatalogue').then(fenetre => {
        if (fenetreajoutcat == 'Catégorie') {
            cy.get('h4').contains(fenetre.fenetrecat.verifcategries).should('be.visible')
        } else if (fenetreajoutcat == 'icone') {
            cy.get('.justify-content-center > .d-flex > .iconify').eq(0).should('be.visible')
        } else if (fenetreajoutcat == 'réinitialiser') {
            cy.get('[class="sc-bczRLJ sc-gKXOVf hiEEob inznaY"]').eq(0).should('be.visible')
        } else if (fenetreajoutcat == 'X fermer') {
            cy.get('[class="sc-bczRLJ sc-gKXOVf hiEEob inznaY"]').eq(2).should('be.visible')
        } else if (fenetreajoutcat == 'Désignation *') {
            cy.get('[class="sc-iBkjds kwBQUT"]').contains(fenetre.fenetrecat.verifDésignation).should('be.visible')
        } else if (fenetreajoutcat == 'input désignation') {
            cy.get('#Désignation').should('be.visible')
        } else if (fenetreajoutcat == 'Ce champ est obligatoire') {
            //click button valider pour afficher msg Ce champ est obligatoire
            cy.get('[class="sc-bczRLJ cueLih"]').click()
            cy.get('[class="invalid-feedback"]').contains(fenetre.fenetrecat.verifchampobligatoire).should('be.visible')
        } else if (fenetreajoutcat == "Nom d'affichage") {
            cy.get('[class="sc-iBkjds kwBQUT"]').contains(fenetre.fenetrecat.verifNomaffichage).should('be.visible')
        } else if (fenetreajoutcat == 'Avancé nom affichage') {
            cy.get(':nth-child(2) > .d-flex > span').contains(fenetre.fenetrecat.verifAvancéaffich).should('be.visible')
        } else if (fenetreajoutcat == 'input nom affichage') {
            cy.get('#Name').should('be.visible')
        } else if (fenetreajoutcat == 'Description') {
            cy.get('[class="sc-iBkjds kwBQUT"]').contains(fenetre.fenetrecat.verifDescription).should('be.visible')
        } else if (fenetreajoutcat == 'Avancé Description') {
            cy.get(':nth-child(3) > .justify-content-between > span').contains(fenetre.fenetrecat.verifAvancédescription).should('be.visible')
        } else if (fenetreajoutcat == 'input Description') {
            cy.get('[name="Desription"]').should('be.visible')
        } else if (fenetreajoutcat == 'Maximum number of characters 2000') {
            cy.get('.ml-3').contains(fenetre.fenetrecat.verifMaximum).should('be.visible')
        } else if (fenetreajoutcat == 'Catégorie parente') {
            cy.get('[class="sc-iBkjds kwBQUT"]').contains(fenetre.fenetrecat.verifCatégparente).should('be.visible')
        } else if (fenetreajoutcat == 'input Catégorie parente') {
            cy.get('[class=" css-g1d714-ValueContainer"]').should('be.visible')
        } else if (fenetreajoutcat == 'Catégories de liaison') {
            cy.get('[class="sc-iBkjds kwBQUT"]').contains(fenetre.fenetrecat.verifCatégliaison).should('be.visible')
        } else if (fenetreajoutcat == 'input Catégories de liaison') {
            cy.get('[class=" css-6j8wv5-Input"]').should('be.visible')
        } else if (fenetreajoutcat == 'Couleur') {
            cy.get('[class="sc-iBkjds kwBQUT"]').contains(fenetre.fenetrecat.verifCouleur).should('be.visible')
        } else if (fenetreajoutcat == 'input Couleur') {
            cy.get('[name="Couleur"]').should('be.visible')
        } else if (fenetreajoutcat == 'Code Hexadécimal') {
            cy.get('[class="sc-iBkjds kwBQUT"]').contains(fenetre.fenetrecat.verifCodeHexadécimal).should('be.visible')
        } else if (fenetreajoutcat == 'input Code Hexadécimal') {
            cy.get('[name="Hexadecimal"]').should('be.visible')
        } else if (fenetreajoutcat == 'Ordre') {
            cy.get('[class="sc-iBkjds kwBQUT"]').contains(fenetre.fenetrecat.verifOrdre).should('be.visible')
        } else if (fenetreajoutcat == 'input Ordre') {
            cy.get('[name="Ordre"]').should('be.visible')
        } else if (fenetreajoutcat == 'Visibilité') {
            cy.get('[class="sc-iBkjds kwBQUT"]').contains(fenetre.fenetrecat.verifVisibilité).should('be.visible')
        } else if (fenetreajoutcat == 'Image') {
            cy.get('.pt-4 > .sc-iBkjds').should('be.visible')
        } else if (fenetreajoutcat == 'Avancé image') {
            cy.get('.pt-4 > div').contains(fenetre.fenetrecat.verifAvancéimage).should('be.visible')
        }
    })
})

//test fonctionnement ajouter un article
Cypress.Commands.add('fenetrearticle', (fentreajoutarticle) => {
    cy.fixture('article').then(saisir => {
        if (fentreajoutarticle == 'Désignation') {
            cy.get('#designation').type(saisir.input.désignation)
        } else if (fentreajoutarticle == "Nom d'affichage") {
            cy.get('#displayName').type(saisir.input.Nomaffichage)
        } else if (fentreajoutarticle == 'Description') {
            cy.get('#Description').type(saisir.input.Description)
        } else if (fentreajoutarticle == 'Catégorie parente') {
            cy.get('[class=" css-g1d714-ValueContainer"]').eq(0).type(saisir.input.Catégorieparente)
        } else if (fentreajoutarticle == 'Catégories de liaison') {
            cy.get('[class=" css-1s2u09g-control"]').type(saisir.input.Catégoriesdeliaison)
        } else if (fentreajoutarticle == 'Prix') {
            cy.get('#Price').type(saisir.input.Prix)
        } else if (fentreajoutarticle == 'Allergènes') {
            cy.get('[class=" css-g1d714-ValueContainer"]').type(saisir.input.Allergènes)
        } else if (fentreajoutarticle == 'Couleur') {
            cy.get('input[id="Hexadecimal"]').clear().type(saisir.input.Couleur)
        } else if (fentreajoutarticle == 'Calories') {
            cy.get('#calories').type(saisir.input.Calories)
        } else if (fentreajoutarticle == 'Points fidélite') {
            cy.get('#fidelity').type(saisir.input.Pointsfidélite)
        } else if (fentreajoutarticle == 'Options') {
            cy.get('#Options').type(saisir.input.Options)
        } else if (fentreajoutarticle == 'Ordre') {
            cy.get('#Ordre').type(saisir.input.Ordre)
        } else if (fentreajoutarticle == 'Visibilité') {
            cy.get('[class="card"]').type(saisir.input.Visibilité)
        }
    })
    Cypress.Commands.add('image', (ajouimage) => {
        if (ajouimage == 'Image') {
            cy.get('#file-').attachFile('FAMND98.png')
            cy.wait(1000)
        }
    })
})

Cypress.Commands.add('language', (nomaffichage) => {
    cy.fixture('article').then(ajoutnomaffichage => {
        if (nomaffichage == 'POS') {
            cy.get('#POS-Francais').type(ajoutnomaffichage.input.nomfrancais)
            cy.get('#POS-Anglais').type(ajoutnomaffichage.input.nomanglais)
            cy.get('#POS-Italien').type(ajoutnomaffichage.input.nomaitalien)
            cy.get('#POS-Allemand').type(ajoutnomaffichage.input.nomallemand)
        } else if (nomaffichage == 'SCO') {
            cy.get('#SCO-Francais').type(ajoutnomaffichage.input.nomfrancais)
            cy.get('#SCO-Anglais').type(ajoutnomaffichage.input.nomanglais)
            cy.get('#SCO-Italien').type(ajoutnomaffichage.input.nomaitalien)
            cy.get('#SCO-Allemand').type(ajoutnomaffichage.input.nomallemand)
        } else if (nomaffichage == 'UBER') {
            cy.get('#UBER-Francais').type(ajoutnomaffichage.input.nomfrancais)
            cy.get('#UBER-Anglais').type(ajoutnomaffichage.input.nomanglais)
            cy.get('#UBER-Italien').type(ajoutnomaffichage.input.nomaitalien)
            cy.get('#UBER-Allemand').type(ajoutnomaffichage.input.nomallemand)
        } else if (nomaffichage == 'KIOSK') {
            cy.get('#KIOSK-Francais').type(ajoutnomaffichage.input.nomfrancais)
            cy.get('#KIOSK-Anglais').type(ajoutnomaffichage.input.nomanglais)
            cy.get('#KIOSK-Italien').type(ajoutnomaffichage.input.nomaitalien)
            cy.get('#KIOSK-Allemand').type(ajoutnomaffichage.input.nomallemand)
        } else if (nomaffichage == 'SCO') {
            cy.get('#SCO-Francais').type(ajoutnomaffichage.input.nomfrancais)
            cy.get('#SCO-Anglais').type(ajoutnomaffichage.input.nomanglais)
            cy.get('#SCO-Italien').type(ajoutnomaffichage.input.nomaitalien)
            cy.get('#SCO-Allemand').type(ajoutnomaffichage.input.nomallemand)
        } else if (nomaffichage == 'SITE') {
            cy.get('[class="d-flex justify-content-center mt-1 mb-1"]').eq(16).type(ajoutnomaffichage.input.nomfrancais)
            cy.get('[class="d-flex justify-content-center mt-1 mb-1"]').eq(17).type(ajoutnomaffichage.input.nomanglais)
            cy.get('[class="d-flex justify-content-center mt-1 mb-1"]').eq(18).type(ajoutnomaffichage.input.nomaitalien)
            cy.get('[class="d-flex justify-content-center mt-1 mb-1"]').eq(19).type(ajoutnomaffichage.input.nomallemand)
        } else if (nomaffichage == 'APPLICATION C&C') {
            cy.get('[class="d-flex justify-content-center mt-1 mb-1"]').eq(20).type(ajoutnomaffichage.input.nomfrancais)
            cy.get('[class="d-flex justify-content-center mt-1 mb-1"]').eq(21).type(ajoutnomaffichage.input.nomanglais)
            cy.get('[class="d-flex justify-content-center mt-1 mb-1"]').eq(22).type(ajoutnomaffichage.input.nomaitalien)
            cy.get('[class="d-flex justify-content-center mt-1 mb-1"]').eq(23).type(ajoutnomaffichage.input.nomallemand)
        } else if (nomaffichage == 'IMPRESSION') {
            cy.get('#Francais-Francais').type(ajoutnomaffichage.input.nomfrancais)
            cy.get('#Anglais-Anglais').type(ajoutnomaffichage.input.nomanglais)
            cy.get('#Italien-Italien').type(ajoutnomaffichage.input.nomaitalien)
            cy.get('#Allemand-Allemand').type(ajoutnomaffichage.input.nomallemand)
        }

    })
})

//////////////Cas des tests : GESTIONS DES ETAPES////////////////
Cypress.Commands.add('ajouteretape', (nouveletape) => {
    cy.fixture('GestionEtape').then(ajouter => {
        if (nouveletape == 'Liste des cartes ') {
            cy.get(ajouter.titlenom.titlespan1).contains(ajouter.nombutton.listedescartes).click()
        } else if (nouveletape == 'Choisir') {
            cy.get(ajouter.titlenom.attribu1).eq(0).contains(ajouter.nombutton.choisir).click()
        } else if (nouveletape == 'Catalogue') {
            cy.get(ajouter.titlenom.titlespan2).contains(ajouter.nombutton.Catalogue).click()
        } else if (nouveletape == 'Etapes') {
            cy.get(ajouter.titlenom.a1).contains(ajouter.nombutton.Etapes).click()
        } else if (nouveletape == 'ajouter une etape ') {
            cy.get(ajouter.titlenom.attribu2).contains(ajouter.nombutton.ajouteruneetsape).click()
        } else if (nouveletape == 'Valider') {
            cy.get(ajouter.titlenom.attribu4).contains(ajouter.nombutton.btnvalider).click()
        }
    })
})

Cypress.Commands.add('saisirinput', (saisir) => {
    cy.fixture('GestionEtape').then(ajouter => {
        if (saisir == 'boisson') {
            cy.get(ajouter.titlenom.idinput).type(ajouter.nombutton.saisirboisson)
        } else if (saisir == 'boisson33cl') {
            cy.get(ajouter.titlenom.attribu3).eq(1).type(ajouter.nombutton.saisirboisson2)
        }
    })
})


// Cypress.Commands.add('AddEtapes', (saisir, designation, nomaffichage, NCMparetape, NCMpararticle, avecprixzero, Prixarticledasnletape) => {
//     cy.fixture('GestionEtape').then(ajouter => {
//         if (saisir === "designation") {
//             cy.get(ajouter.titlenom.idinput).type(designation)
//         } else if (saisir === "nonaffichage") {
//             cy.get('[name="title"]').eq(1).type(nomaffichage)
//         } else if (saisir == "all") {
//             cy.get(ajouter.titlenom.idinput).type(designation)

//             cy.get('[name="title"]').eq(1).type(nomaffichage)

//             cy.get('#title').type(NCMparetape)
//             cy.get('[ class="sc-iqcoie VjgQJ is-touched is-dirty av-valid form-control"]').type(NCMpararticle)
//             cy.get('[class="sc-iqcoie VjgQJ is-touched is-dirty av-valid form-control"]').type(avecprixzero)
//             cy.get('[class="sc-iqcoie VjgQJ is-touched is-dirty av-valid form-control"]').type(Prixarticledasnletape)
//         }
//     })

// })