Feature: test catalogue

test visibilite et design catalogue
Scenario: visibilite catalogue et sous catalogue
Given I open the brawser
Then I verify the 'Catalogue' is visible
When I click button 'Catalogue'
And I verify the 'Catégories' is visible
And I verify the 'Articles' is visible
And I verify the 'Options' is visible
And I verify the 'Balises' is visible
And I verify the 'Workflow' is visible
Then I verify the 'Allergènes' is visible


Scenario: design page categories
Given I click button 'Liste des cartes '
Then I click button 'Choisir'
When I click button 'Catalogue'
And I click button 'Catégories'
Then I verify visibilite 'ETK CATALOGUE '
Then I verify visibilite 'FRANCHISE_SUSHI_MEAL_94400'
And I verify visibilite 'Texte à rajouter dans cette zone'
And I verify visibilite 'Ajouter une catégorie'
And I verify visibilite 'Filtre(s)'
Then I verify visibilite 'Rechercher'
When I verify the visibilite 'checkbox' and clickable
When I verify visibilite 'Désignation'
And I verify visibilite 'Ordre'
And I verify visibilite 'Image'
And I verify visibilite 'Couleur'
And I verify visibilite 'Description'
And I verify visibilite 'Actions'
And I verify visibilite 'dropdown'
Then I verify 'ligne' table
Then I verify 'colonne' table
Then I verify 'Lignes par page' table
Then I verify 'Page 1 sur 13' table
And I click button 'dropdown'
Then I verify 'dropdown-menu' is visible
And I verify 'Afficher tous:' is visible
And I verify 'Désignation' is visible
And I verify 'Ordre' is visible
And I verify 'Image' is visible
And I verify 'Couleur' is visible
And I verify 'Description' is visible
When I click and verify 'three points'
When I click and verify ' Importer fichier excel'
And I click and verify 'Modification multiples'
And I click and verify ' Duppliquer'
And I click and verify 'Supprimer'
Then I click and verify 'popthreepoint'
And I click and verify ' Selectionner categorie'
And I click and verify 'Voir la liste des boutiques'
And I click and verify 'flesh droite'
And I click and verify 'flesh gauche'
And I click and verify 'double flesh droite'
And I click and verify 'double flesh gauche'


Scenario: designe pop up catégorie
Given I click on the button 'Ajouter une catégorie'
When I verify my fenetre 'Catégorie'
Then I verify my fenetre 'icone'
And I verify my fenetre 'réinitialiser'
And I verify my fenetre 'X fermer'
And I verify my fenetre 'Désignation *'
And I verify my fenetre 'input désignation'
And I verify my fenetre 'Ce champ est obligatoire'
And I verify my fenetre "Nom d'affichage"
And I verify my fenetre 'Avancé nom affichage'
And I verify my fenetre 'input nom affichage'
And I verify my fenetre 'Description'
And I verify my fenetre 'Avancé Description'
And I verify my fenetre 'input Description'
And I verify my fenetre 'Maximum number of characters 2000'
And I verify my fenetre 'Catégorie parente'
And I verify my fenetre 'input Catégorie parente'
And I verify my fenetre 'flesh Catégorie parente'
And I verify my fenetre 'Catégories de liaison'
And I verify my fenetre 'input Catégories de liaison'
And I verify my fenetre 'Couleur'
And I verify my fenetre 'input Couleur'
And I verify my fenetre 'Code Hexadécimal'
And I verify my fenetre 'input Code Hexadécimal'
And I verify my fenetre 'Ordre'
And I verify my fenetre 'input Ordre'
And I verify my fenetre 'Visibilité'
And I click on the button 'Avancé'
And I click on the button 'close'
And I verify my fenetre 'Image'
And I verify my fenetre 'Avancé image'

