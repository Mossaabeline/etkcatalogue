import { And, Given, Then, When } from 'cypress-cucumber-preprocessor/steps'


Given('I open the brawser', () => {
    cy.visit('/')
    cy.wait(2000)
})
When('I click button {string}', (choixnamebtn) => {
    cy.selectbtn(choixnamebtn)
})

When('I write the new {string}', (fentreajoutarticle) => {
    cy.fenetrearticle(fentreajoutarticle)
})

Then('I shoose logo {string}', (ajouimage) => {
    cy.image(ajouimage)
})

And('I click the button {string}', (choixbtn) => {
    cy.button(choixbtn)
})

When('I write the language in francais and anglais and italien and almand {string}', (nomaffichage) => {
    cy.language(nomaffichage)
})