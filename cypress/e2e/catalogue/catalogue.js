import { And, Given, Then, When } from 'cypress-cucumber-preprocessor/steps'

Given('I open the brawser', () => {
    cy.visit('/')
    cy.wait(2000)
})

Then('I verify the {string} is visible', (name) => {
    cy.namebutton(name)
})

When('I click button {string}', (choixnamebtn) => {
    cy.selectbtn(choixnamebtn)
})

Then('I verify visibilite {string}', (visible) => {
    cy.testvisibilite(visible)
})

When('I verify the visibilite {string} and clickable', (check) => {
    cy.checkboxbtn(check)
})

Then('I verify {string} table', (ligne) => {
    cy.number(ligne)
})

Then('I verify {string} is visible', (popup) => {
    cy.menudropdown(popup)
})

When('I click and verify {string}', (nom) => {
    switch (nom) {
        case 'three points':
            cy.get('[class="dropdown "]').click()
            cy.get('#dropdownMenuButton1').should('be.visible')
            cy.get('#dropdownMenuButton1').click()
            break
        case ' Importer fichier excel':
            cy.get('[class="dropdown-item "]').contains(' Importer fichier excel').should('be.visible')
            break
        case 'Modification multiples':
            cy.get('[class="dropdown-item"]').contains('Modification multiples').should('be.visible')
            break
        case ' Duppliquer':
            cy.get('[class="dropdown-item"]').contains(' Duppliquer').should('be.visible')
            break
        case 'Supprimer':
            cy.get('[class="dropdown-item"]').contains('Supprimer').should('be.visible')
            cy.get('#dropdownMenuButton1').click()
            break
        case ' Selectionner categorie':
            cy.get('[class="dropdown-item"]').contains(' Selectionner categorie').should('be.visible')
            break
        case 'Voir la liste des boutiques':
            cy.get('[class="dropdown-item"]').contains('Voir la liste des boutiques').should('be.visible')
            break
        case 'popthreepoint':
            cy.get(':nth-child(1) > [style="box-sizing: border-box; flex: 60 0 auto; min-width: 60px; width: 60px; justify-content: start; align-items: center; display: flex;"] > .w-100 > .dropdown > #dropdownMenuButton1').should('be.visible').click()
            break
        case 'flesh droite':
            cy.get('[class="btn"]').eq(2).should('be.visible').dblclick()
            break
        case 'flesh gauche':
            cy.get('[class="btn"]').eq(1).should('be.visible').dblclick({ force: true })
            break
        case 'double flesh droite':
            cy.get('[class="btn"]').eq(3).should('be.visible').click({ force: true })
            break
        case 'double flesh gauche':
            cy.get('[class="btn"]').eq(0).should('be.visible').click({ force: true })
            break
    }
})

Given('I click on the button {string}', (categories) => {
    cy.ajoutercategories(categories)
})

When('I verify my fenetre {string}', (fenetreajoutcat) => {
    cy.fentreajouter(fenetreajoutcat)
})