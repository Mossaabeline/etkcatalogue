import { And, Given, Then, When } from 'cypress-cucumber-preprocessor/steps'

Given('I open the brawser', () => {
    cy.visit('/')
    cy.wait(1000)
})

And('I verify the {string} is visible', (choixsize) => {
    cy.fontezise(choixsize)
        // cy.get('h1').should('have.text', 'ETK|Article DB').then(sizes => {
        //     expect(sizes).to.have.css('color', 'rgb(255, 255, 255)')
        //     expect(sizes).to.have.css('font-weight', '500')
        //     expect(sizes).to.have.css('line-height', '43.2px')
        //     expect(sizes).to.have.css('Margin', '0px 0px 8px')
        //     expect(sizes).to.have.css('Padding', '8px 0px 16px')
        //     expect(sizes).to.have.css('justify-content', 'center')
        // })
})

Then('I verify design {string}', (shoismenu) => {
    cy.menudesign(shoismenu)
})

And('I verify the styles {string}', (page) => {
    cy.choixpages(page)
})

Then('I verify the design {string}', (ecreture) => {
    switch (ecreture) {
        case 'numero':
            cy.get('strong:eq(9)').should('have.css', 'color', 'rgb(41, 41, 41)').and('have.css', 'font-size', '16px').and('have.css', 'font-weight', '700').and('have.css', 'line-height', '24px').and('have.css', 'text-align', 'left')
            break
        case 'Sélectionner une carte':
            cy.get('div').contains('Sélectionner une carte').should('have.css', 'color', 'rgb(0, 123, 255)').and('have.css', 'font-size', '16px').and('have.css', 'font-weight', '400').and('have.css', 'text-decoration', 'none solid rgb(0, 123, 255)')
            break

        case 'footre':
            cy.get('[class="d-flex col-sm pb-md-row row"]').should('be.visible')
            cy.get('[class="d-flex col-sm pb-md-row row"]').should('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'font-weight', '400').and('have.css', 'font-size', '16px').and('have.css', 'padding-left', '15px').and('have.css', 'padding-right', '15px')
            break
        case 'footre image':
            cy.get('[class="mx-3 mt-2"]').should('be.visible')
            cy.get('[class="mx-3 mt-2"]').should('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'font-weight', '400').and('have.css', 'font-size', '16px').and('have.css', 'margin-left', '16px').and('have.css', 'margin-right', '16px').and('have.css', 'height', '50px')
            break
        case 'Numéro Support':
            cy.get('div').contains("Numéro Support").should('be.visible')
            cy.get('div').contains("Numéro Support").should('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'font-size', '16px').and('have.css', 'font-weight', '400').and('have.css', 'line-height', '24px')
            break
        case '01 80 81 70 00':
            cy.get('div').contains('01 80 81 70 00').should('have.text', '01 80 81 70 00').should('be.visible')
            cy.get('div').contains('01 80 81 70 00').should('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'font-size', '16px').and('have.css', 'font-weight', '400').and('have.css', 'line-height', '24px').and('have.css', 'display', 'block')
            break
        case '© 2022 Aureskonnect |':
            cy.get('[class="mt-4 mx-1 mb-md-4"]').contains('© 2022 Aureskonnect |').should('have.text', '© 2022 Aureskonnect |').should('be.visible')
            cy.get('[class="mt-4 mx-1 mb-md-4"]').contains('© 2022 Aureskonnect').should('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'font-size', '16px').and('have.css', 'font-weight', '400').and('have.css', 'margin-left', '4px').and('have.css', 'margin-right', '4px')
            break
        case "Conditions d'utilisation":
            cy.get('[class="mt-4 mx-1 mb-md-4"]').contains("Conditions d'utilisation").should('have.text', "Conditions d'utilisation").should('be.visible')
            cy.get('[class="mt-4 mx-1 mb-md-4"]').contains("Conditions d'utilisation").should('have.css', 'color', 'rgb(0, 123, 255)').and('have.css', 'font-size', '16px').and('have.css', 'font-weight', '400').and('have.css', 'line-height', '24px').and('have.css', 'cursor', 'pointer')
            break
        case '| Règles de confidentialité':
            cy.get('[class="mt-4 mx-1 mb-md-4"]').contains('Règles de confidentialité').should('have.text', '| Règles de confidentialité').should('be.visible')
            cy.get('[class="mt-4 mx-1 mb-md-4"]').contains("Conditions d'utilisation").should('have.css', 'color', 'rgb(0, 123, 255)').and('have.css', 'font-size', '16px').and('have.css', 'font-weight', '400').and('have.css', 'line-height', '24px').and('have.css', 'cursor', 'pointer')
            break
    }
})

And('I verify the navigbar {string}', (barheader) => {
    switch (barheader) {
        case 'header':
            cy.get('[class="navbar-header"]').should('have.css', 'position', 'static')
            cy.get('[class="footer mt-2"]').should('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'font-weight', '400').and('have.css', 'font-size', '16px').and('have.css', 'position', 'static').and('have.css', 'width', '1848px')
            break
        case 'deconnecter':
            cy.get('#page-header-user-dropdown').should('have.css', 'position', 'static')
            cy.get('#page-header-user-dropdown').should('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'font-weight', '400').and('have.css', 'font-size', '16px').and('have.css', 'cursor', 'pointer').and('have.css', 'height', '54px')
            break
        case 'language':
            cy.get('[class="btn header-item waves-effect"]').eq(0).should('have.css', 'position', 'static')
            cy.get('[class="d-inline-block dropdown"]').eq(0).click()
            cy.get('[class="language-switch dropdown-menu dropdown-menu-right show"]').click()
            cy.get('[class="d-inline-block dropdown"]').eq(0).should('have.css', 'color', 'rgb(33, 37, 41)').and('have.css', 'font-weight', '400').and('have.css', 'font-size', '16px').and('have.css', 'position', 'relative').and('have.css', 'line-height', '24px')
            break
        case 'button':
            cy.get('[xmlns="http://www.w3.org/2000/svg"]').should('have.css', 'position', 'static')
            cy.get('[class="sc-bczRLJ sc-gKXOVf hiEEob inznaY m-0 border-none__clz"]').eq(0).should('have.css', 'color', 'rgb(43, 40, 40)').and('have.css', 'font-weight', '400').and('have.css', 'font-size', '16px').and('have.css', 'line-height', '24px').and('have.css', 'cursor', 'pointer')
            break
        case 'flech':
            cy.get('[xmlns="http://www.w3.org/2000/svg"]').should('be.visible')
            cy.get('[xmlns="http://www.w3.org/2000/svg"]').should('have.css', 'color', 'rgb(255, 255, 255)').and('have.css', 'font-weight', '400').and('have.css', 'font-size', '16px').and('have.css', 'line-height', '27px').and('have.css', 'cursor', 'pointer')
    }
})