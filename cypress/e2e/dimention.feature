Feature: size page menu

Scenario: add a new size menu
Given I open the brawser
Then I verify design 'menu ouvert'
When I verify design 'menu fermer'
And I verify the 'ETK|Article DB' is visible
And I verify the 'Accueil' is visible
And I verify the 'Liste des cartes ' is visible
Then I verify the 'Catalogue' is visible
Then I verify the 'Modules' is visible
When I verify the 'Configuration' is visible
And I verify the styles 'CATEGORIES'
And I verify the styles 'ARTICLES'
And I verify the styles 'BALISES'
And I verify the styles 'FLUX DE TRAVAIL'
And I verify the styles 'CHOISIR UNE CARTE'
Then I verify the design 'numero'
Then I verify the design 'Sélectionner une carte'
Then I verify the design 'footre'
When I verify the design 'footre image'
When I verify the design 'Numéro Support'
And I verify the design '01 80 81 70 00'
And I verify the design '© 2022 Aureskonnect |'
Then I verify the design "Conditions d'utilisation"
Then I verify the design '| Règles de confidentialité'
And I verify the navigbar 'header'
And I verify the navigbar 'deconnecter'
When I verify the navigbar 'language'
Then I verify the navigbar 'button'
And I verify the navigbar 'flech'


